# CI/CD Live Traces Sandbox

This is a test project that we use as a first step to incrementally roll out cloud native build logs.

See https://gitlab.com/gitlab-org/gitlab/-/issues/217988#rollout-plan

